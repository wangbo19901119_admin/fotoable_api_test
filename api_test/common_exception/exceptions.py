class FileIsNoneException(Exception):
    def __str__(self):
        return '批量文件内容为空,请与模版进行核对!'


class ParamsMissedException(Exception):
    def __init__(self, *args):
        self.params = args[0] if len(args) == 1 else ','.join(*args).strip(',')

    def __str__(self):
        return f'缺少参数{self.params},请检查!'


class ParamsIsNullException(Exception):
    def __init__(self, *args):
        self.params = args[0] if len(args) == 1 else ','.join(*args).strip(',')

    def __str__(self):
        return f'参数{self.params}为空,请检查!'


class ParamsTypeErrorException(Exception):
    def __init__(self, params):
        self.params = params

    def __str__(self):
        return '参数' + self.params + '类型错误,请检查!'


class NoPerMissionException(Exception):
    def __init__(self, user):
        self.user = user

    def __str__(self):
        return '用户' + self.user + '无操作权限,请更换用户!'


class FileNotFoundException(Exception):
    def __init__(self, file_id, project_id):
        self.file_id = file_id
        self.project_id = project_id

    def __str__(self):
        return '项目' + self.project_id + '下未找到文件' + self.file_id + ',请检查!'


class ObjectNotFoundException(Exception):
    def __init__(self, desc):
        self.desc = desc

    def __str__(self):
        return self.desc + '不存在!'


class ObjectIsDeletedException(Exception):
    def __init__(self, desc):
        self.desc = desc

    def __str__(self):
        return self.desc + '已禁用,请联系管理员!'
