import logging
from django_bulk_update.helper import bulk_update
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from api_test.common.api_response import JsonResponse
from api_test.models import CaseLibrary, User
from api_test.serializers import CaseLibraryDeserializer, CaseLibrarySerializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class FunctionalCaseLibList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="用例名称"),
        openapi.Parameter(name="module", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="所属模块")
    ])
    def get(self, request):
        """
        获取功能测试公共用例
        """
        name = request.GET.get("name")
        module = request.GET.get("module")
        page_size = int(request.GET.get("page_size", 10))
        page = int(request.GET.get("page", 1))
        if not isinstance(page, int) or not isinstance(page_size, int):
            return JsonResponse(code="999996", msg="参数有误!")
        obi = CaseLibrary.objects.filter(status=True)
        if name:
            obi = obi.filter(name__contains=name)
        if module:
            obi = obi.filter(module=module)
        obi = obi.order_by("id")
        paginator = Paginator(obi, page_size)  # paginator对象
        page_count = paginator.num_pages  # 总页数
        total = paginator.count  # 总数
        try:
            obm = paginator.page(page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = CaseLibrarySerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data, "page_sizes": page_size, "total": total,
                                  'current_page': page, 'page_count': page_count},
                            code="999999", msg="查询成功!")


class AddLibraryCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['name', 'steps', 'stage', 'level']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['name', 'steps', 'stage', 'level'],
        properties={
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="用例标题"),
            'module': openapi.Schema(type=openapi.TYPE_STRING, description="所属模块"),
            'steps': openapi.Schema(type=openapi.TYPE_STRING, description="预期结果"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="描述"),
            'level': openapi.Schema(type=openapi.TYPE_STRING, description="优先级"),
            'stage': openapi.Schema(type=openapi.TYPE_STRING, description="适应阶段"),
            'preconditions': openapi.Schema(type=openapi.TYPE_STRING, description="前置条件"),
            'keywords': openapi.Schema(type=openapi.TYPE_STRING, description="关键字"),
        },
    ))
    def post(self, request):
        """
        功能测试用例库新增用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        count = CaseLibrary.objects.filter(name=data["name"], status=True).count()
        if count > 0:
            return JsonResponse(code="999997", msg="存在相同名称!")
        else:
            with transaction.atomic():  # 执行错误后，帮助事务回滚
                serialize = CaseLibraryDeserializer(data=data)
                if serialize.is_valid():
                    serialize.save(userUpdate=User.objects.get(username=request.user))
                    return JsonResponse(code="999999", msg=f"用例库新增用例成功!", data={"case_id": serialize.data.get("id")})
                else:
                    return JsonResponse(code="999996", msg="参数有误!", data=serialize.errors)


class EditLibraryCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['id', 'name', 'steps', 'stage', 'level']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'name', 'steps', 'stage', 'level'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="用例id"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="用例标题"),
            'module': openapi.Schema(type=openapi.TYPE_STRING, description="所属模块"),
            'steps': openapi.Schema(type=openapi.TYPE_STRING,  description="预期结果"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="描述"),
            'level': openapi.Schema(type=openapi.TYPE_STRING, description="优先级"),
            'stage': openapi.Schema(type=openapi.TYPE_STRING, description="适应阶段"),
            'preconditions': openapi.Schema(type=openapi.TYPE_STRING, description="前置条件"),
            'keywords': openapi.Schema(type=openapi.TYPE_STRING, description="关键字"),
        },
    ))
    def post(self, request):
        """
        更新功能测试库用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        count = CaseLibrary.objects.filter(name=data["name"], status=True).exclude(id=data["id"]).count()
        if count > 0:
            return JsonResponse(code="999997", msg="存在相同名称!")
        try:
            obi = CaseLibrary.objects.get(id=data["id"])
            obi.userUpdate = User.objects.get(username=request.user)
        except ObjectDoesNotExist:
            return JsonResponse(code="999990", msg="case不存在!")
        with transaction.atomic():  # 执行错误后，帮助事务回滚
            serialize = CaseLibraryDeserializer(data=data)
            if serialize.is_valid():
                serialize.update(instance=obi, validated_data=data)
                return JsonResponse(code="999999", msg=f"用例编辑成功!")
            else:
                return JsonResponse(code="999996", msg="用例更新失败!", data=serialize.errors)


class DelLibraryCase(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            if not data.get("ids"):
                return JsonResponse(code="999996", msg="参数有误!")
            if not isinstance(data["ids"], list):
                return JsonResponse(code="999996", msg="参数有误!")
            for i in data["ids"]:
                if not isinstance(i, int):
                    return JsonResponse(code="999996", msg="参数有误!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['ids'],
        properties={
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                  description="待删除待用例id列表"),
        },
    ))
    def post(self, request):
        """
        删除功能测试用例库用例
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        id_list = data.get('ids')
        obj = CaseLibrary.objects.filter(id__in=id_list, status=True)
        if len(obj) < len(id_list):
            return JsonResponse(code="999996", msg="请检查ids数据,存在不存在的id或已删除的数据!")
        for i in obj:
            i.status = False
        bulk_update(obj, update_fields=['status'])
        return JsonResponse(code="999999", msg="删除成功!")
