import logging
import jenkins
import json
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import SysConfig
from datetime import datetime
import time

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。

conf = SysConfig.objects.filter(key='jenkins')
if conf:
    value = json.loads(conf[0].value)
    url, username, password = value.get('url'), value.get('username'), value.get('password')
else:
    url, username, password = 'http://10.31.4.246:8081', 'admin', 'admin'
server = jenkins.Jenkins(url=url, username=username, password=password)


class JenkinsJobList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @catch_exception
    def post(self, request):
        """
        获取jenkins任务列表及参数
        """
        # 获取jenkins任务自动化测试任务列表及任务参数
        jobs = server.get_jobs()
        job_list = []
        i = 1
        for job in jobs:
            if 'autotest' in job.get('name'):
                res = server.get_job_info(job.get('name'))
                params = {param.get('name'): param.get('choices') for param in
                          res.get('actions')[0].get('parameterDefinitions')}
                job_dict = dict(id=i, name=res.get('displayName'), description=res.get('description'),
                                disabled=res.get('disabled'), params=params, url=res.get('url'))
                job_list.append(job_dict)
                i += 1
        return JsonResponse(code="999999", msg="获取列表成功!",
                            data=dict(data=job_list, current_page=1, page_sizes=10, total=len(job_list)))


class ExecuteJenkinsJobList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @catch_exception
    def post(self, request):
        """
        执行jenkins任务
        """
        data = JSONParser().parse(request)
        name = data.get('name')
        params = data.get('params')
        if not name:
            return JsonResponse(code="999995", msg="任务名称不能为空!")
        if not params:
            return JsonResponse(code="999995", msg="任务名称参数不能为空!")
        # 获取job最后一次build状态，如未完成则继续上次的构建
        last_build_number = server.get_job_info(name)['lastBuild']['number']
        build_state = server.get_build_info(name, last_build_number)['result']
        if not build_state:
            return JsonResponse(code="900000", msg="有正在进行的构建任务!")
        else:
            res = server.build_job(name, params, token='11177b54cabb7c920d6fe1680c433b5fd8')
        return JsonResponse(code="999999", msg="执行成功!", data=res)


class GetJenkinsBuildsList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def get_build_time(name, build):
        return server.get_build_info(name, build['id']).get('timestamp')

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="任务名称"),
        openapi.Parameter(name="app", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="app名称"),

    ])
    @catch_exception
    def get(self, request):
        """
        执行jenkins任务
        """
        name = request.GET.get("name")
        page_size = int(request.GET.get("page_size", 10))
        rid = request.GET.get("id")
        page = int(request.GET.get("page", 1))
        if not name:
            return JsonResponse(code="999995", msg="任务名称不能为空!")
        # 获取job最后一次build状态，如未完成则继续上次的构建
        if rid:
            builds = [dict(id=build['number'], url=build['url'] + 'allure', task=name) for build in
                      server.get_job_info(name).get('builds') if build['number'] == int(rid)]
        else:
            builds = [dict(id=build['number'], url=build['url'] + 'allure', task=name) for build in server.get_job_info(name).get('builds')]
        for build in builds[(page-1)*page_size: page * page_size]:
            build['time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(self.get_build_time(name, build)/1000))
        return JsonResponse(data={"data": builds, "page_sizes": page_size, "total": len(builds),
                                  'current_page': page, 'page_count': len(builds) % page_size},
                            code="999999", msg="查询成功!")
