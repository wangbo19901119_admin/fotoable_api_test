from django.http import HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from api_test.common.decorator import catch_exception
from api_test.models import HelpDocumentInfo
from api_test.serializers import HelpDocumentSerializer


class HelpDocument(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @catch_exception
    def get(self, request):
        """
        获取帮助文档内容
        """
        # help_id = request.GET.get('help_id')
        # if not help_id:
        #     raise ParamsMissedException('help_id')

        help_obj = HelpDocumentInfo.objects.get(status=True)

        serialize = HelpDocumentSerializer(help_obj)

        html = serialize.data.get('help_document')
        return HttpResponse(html)
