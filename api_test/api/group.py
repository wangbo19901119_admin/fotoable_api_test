import logging
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from api_test.common.api_response import JsonResponse
from api_test.models import ApiGroupLevelFirst
from api_test.serializers import ApiGroupLevelFirstSerializer, ApiGroupLevelFirstDeserializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class Group(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        # openapi.Parameter(name="project_id", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="项目id"),
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="模块名称"),
    ])
    def get(self, request):
        """
        接口模块
        """
        try:
            page_size = int(request.GET.get("page_size", 1000))
            current_page = int(request.GET.get("page", 1))
        except (TypeError, ValueError):
            return JsonResponse(code="999985", msg="page and page_size must be integer!")
        name = request.GET.get("name")
        obi = ApiGroupLevelFirst.objects.filter(status=True)
        if name:
            obi = obi.filter(name__icontains=name).order_by("id")
        obi = obi.order_by("id")

        paginator = Paginator(obi, page_size)  # paginator对象
        page_sizes = paginator.num_pages  # 总页数
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = ApiGroupLevelFirstSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": current_page,
                                  "total": total,
                                  "page_sizes": page_sizes,
                                  }, code="999999", msg="成功")

        # return JsonResponse(data=dict(data=serialize.data), code="999999", msg="成功!")


class AddGroup(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 必传参数 name
            if not data.get("name"):
                return JsonResponse(code="999996", msg="参数有误!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['name'],
        properties={
            # 'project_id': openapi.Schema(type=openapi.TYPE_INTEGER, description="项目id"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="接口模块名称"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="接口模块描述")
        },
    ))
    def post(self, request):
        """
        新增接口模块
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        count = ApiGroupLevelFirst.objects.filter(name=data["name"]).count()
        if count > 0:
            return JsonResponse(code="999997", msg="存在相同名称！")
        else:
            # 反序列化
            serializer = ApiGroupLevelFirstDeserializer(data=data)
            # 校验反序列化正确，正确则保存，外键为project
            if serializer.is_valid():
                serializer.save()
            else:
                return JsonResponse(code="999998", msg="新增模块失败!")
            # 新增接口操作
            # record_dynamic(_type="添加", operationObject="接口模块", user=request.user.pk,
            #                data="新增接口模块“%s”" % data["name"])
            return JsonResponse(data={
                "group_id": serializer.data.get("id")
            }, code="999999", msg="新增模块成功!")


class UpdateNameGroup(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 必传参数 name
            if not data.get("id") or not data.get("name"):
                return JsonResponse(code="999996", msg="参数有误!")
            # 校验project_id, id类型为int
            if not isinstance(data["id"], int):
                return JsonResponse(code="999996", msg="参数有误!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'name'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="接口模块id"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="接口模块名称"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="接口模块描述")
        },
    ))
    def post(self, request):
        """
        修改接口模块名称
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        try:
            # pro_data = Project.objects.get(id=data["project_id"])
            if not request.user.is_superuser:
                return JsonResponse(code="999983", msg="无操作权限！")
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg="项目不存在!")
        # pro_data = ProjectListSerializer(pro_data)
        # if not pro_data.data["status"]:
        #     return JsonResponse(code="999985", msg="该项目已禁用")
        try:
            obj = ApiGroupLevelFirst.objects.get(id=data["id"])
        except ObjectDoesNotExist:
            return JsonResponse(code="999991", msg="模块不存在!")
        serializer = ApiGroupLevelFirstDeserializer(data=data)
        if serializer.is_valid():
            serializer.update(instance=obj, validated_data=data)
        else:
            return JsonResponse(code="999998", msg="更新模块失败!")
        # record_dynamic(project=None, _type="修改", operationObject="接口模块", user=request.user.pk,
        #                data="修改接口模块“%s”" % data["name"])
        return JsonResponse(code="999999", msg="更新模块成功!")


class DelGroup(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            if not data.get("ids"):
                return JsonResponse(code="999996", msg="参数有误!")
            # 校验project_id, id类型为int
            if not isinstance(data["ids"], list):
                for i in data["ids"]:
                    if not isinstance(i, int) or not isinstance(int(i), int):
                        return JsonResponse(code="999995", msg="参数有误！")
                return JsonResponse(code="999995", msg="参数有误！")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['ids'],
        properties={
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                  description="待删除待接口模块 id列表"),
        },
    ))
    def post(self, request):
        """
        删除(禁用)接口模块名称
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        # try:
        #     pro_data = Project.objects.get(id=data["project_id"])
        #     if not request.user.is_superuser and pro_data.user.is_superuser:
        #         return JsonResponse(code="999983", msg="无操作权限！")
        # except ObjectDoesNotExist:
        #     return JsonResponse(code="999995", msg="项目不存在!")
        # pro_data = ProjectListSerializer(pro_data)
        # if not pro_data.data["status"]:
        #     return JsonResponse(code="999985", msg="该项目已禁用")
        # 根据项目id和模块 id查找，若存在则删除
        try:
            for j in data["ids"]:
                obj = ApiGroupLevelFirst.objects.get(id=j)
                if obj:
                    # obj.delete()  # 删除
                    obj.status = False
                    obj.save()
                    # record_dynamic(_type="禁用", operationObject="接口模块", user=request.user.pk, data="禁用接口模块“%s”" % obj.name)
            return JsonResponse(code="999999", msg="删除模块成功！")
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg="模块不存在！")
