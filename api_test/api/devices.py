import logging
from datetime import datetime

import requests
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator
from django.db import transaction
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView

from api_test.common.api_response import JsonResponse
from api_test.common.decorator import catch_exception
from api_test.models import Devices, App
from api_test.serializers import DeviceDeserializer, DeviceSerializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class AddDeviceView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        验证参数
        """
        # 必传参数
        for param in ["desc", "port", "platformName", "deviceName", "noReset", "platformVersion",
                      "automationName",  "newCommandTimeout", "bootstrapPort"]:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空！")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=["desc", "port", "platformName", "deviceName", "noReset", "platformVersion",
                  "automationName", "newCommandTimeout", "bootstrapPort"],
        properties={
            'desc': openapi.Schema(type=openapi.TYPE_STRING, description="手机备注"),
            'port': openapi.Schema(type=openapi.TYPE_STRING, description="appium服务端口"),
            'platformName': openapi.Schema(type=openapi.TYPE_STRING, description="手机系统"),
            'deviceName': openapi.Schema(type=openapi.TYPE_STRING, description="设备名称"),
            'noReset': openapi.Schema(type=openapi.TYPE_STRING, description="是否充值"),
            'platformVersion': openapi.Schema(type=openapi.TYPE_STRING, description="系统版本"),
            'automationName': openapi.Schema(type=openapi.TYPE_STRING, description="自动化名称"),
            'newCommandTimeout': openapi.Schema(type=openapi.TYPE_STRING, description="超时时间"),
            'bootstrapPort': openapi.Schema(type=openapi.TYPE_STRING, description="设备与appium交互端口"),
        },
    ))
    def post(self, request):
        """
        手机device信息新增
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        data['createTime'] = datetime.now()
        data['lastUpdateTime'] = datetime.now()
        device_serializer = DeviceDeserializer(data=data)
        try:
            Devices.objects.get(desc=data["desc"])
            return JsonResponse(code="999997", msg="存在相同名称")
        except ObjectDoesNotExist:
            with transaction.atomic():
                if device_serializer.is_valid():
                    device_serializer.save()
                    pid = device_serializer.data.get("id")
                    return JsonResponse(data={
                        "id": pid
                    }, code="999999", msg="手机device信息新增成功")
                else:
                    return JsonResponse(code="999998", msg="手机device新增失败", data={"errmsg": device_serializer.errors})


class DeviceLisView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def get_device_status(serial):
        url = 'http://192.168.29.34:7100/api/v1/devices'
        token = '0edf9f380f7c41f2803066d16e4b8805fc9a318098554f82aaabc3f2f5aaac71'
        headers = {'Authorization': 'Bearer ' + token}
        requests.DEFAULT_RETRIES = 5
        s = requests.session()
        s.keep_alive = False
        response = requests.get(url, headers=headers)
        device_list = []
        if response.status_code == 200:
            for device in response.json().get('devices'):
                if device.get('using'):
                    src = 'https://www.testin.cn/skin_v3/static/images/real_machine/occupy_icon.png'
                else:
                    src = 'https://www.testin.cn/skin_v3/static/images/real_machine/free_icon.png'
                device_list.append(dict(model=device.get('serial'), connect=device.get("remoteConnect"),
                                        present=device.get('present'), using=device.get('using'), src=src))
            for device in device_list:
                if device.get('model') == serial:
                    del device['model']
                    return device

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'desc': openapi.Schema(type=openapi.TYPE_STRING, description="手机名称"),
            'usable': openapi.Schema(type=openapi.TYPE_BOOLEAN, description="可用的"),
            'brand': openapi.Schema(type=openapi.TYPE_OBJECT, description="手机品牌"),
            'screen': openapi.Schema(type=openapi.TYPE_OBJECT, description="手机分辨率"),
            'type': openapi.Schema(type=openapi.TYPE_STRING, description="使用类型"),
            'model': openapi.Schema(type=openapi.TYPE_STRING, description="手机型号"),
            'deviceName': openapi.Schema(type=openapi.TYPE_STRING, description="设备名称"),
            'system': openapi.Schema(type=openapi.TYPE_OBJECT, description="系统版本"),
            'page': openapi.Schema(type=openapi.TYPE_INTEGER, description="当前页数"),
            'page_size': openapi.Schema(type=openapi.TYPE_INTEGER, description="每页最多显示的host数"),
        },
    ))
    @catch_exception
    def post(self, request):
        """
        获取手机device信息
        """
        data = JSONParser().parse(request)
        try:
            page_size = int(data.get("page_size", 30))
            current_page = int(data.get("page", 1))
        except (TypeError, ValueError):
            return JsonResponse(code="999995", msg="page and page_size must be integer！")
        desc = data.get("desc")
        d_type = data.get("type", 'web')
        device_name = data.get("deviceName")
        brand = data.get("brand")
        screen = data.get("screen")
        system = data.get('system')
        model = data.get('model')
        obi = Devices.objects
        usable = data.get('usable')
        if desc:
            obi = obi.filter(desc=desc.strip()).all()
        if model:
            obi = obi.filter(model=model).all()
        if brand:
            obi = obi.filter(brand__in=brand).all()
        if screen:
            obi = obi.filter(screen__in=screen).all()
        if system:
            if system['android'] or system['ios'] or system['harmony']:
                version = []
                for v in system.values():
                    version += v
                obi = obi.filter(platformVersion__in=version).all()
        if device_name:
            obi = obi.filter(deviceName=device_name.strip()).all()
        obi = obi.order_by("id")
        paginator = Paginator(obi, page_size)  # paginator对象
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = DeviceSerializer(obm, many=True)
        device_list = []
        if d_type == 'web':
            device_list = serialize.data
            for data in device_list:
                status = self.get_device_status(data['deviceName'])
                if status:
                    data.update(status)
                else:
                    data.update(dict(connect=False, present=False, using=False, src="https://www.testin.cn/skin_v3/static/images/real_machine/free_icon.png"))
            if usable:
                device_list = [i for i in data if not i.get('present') and not i.get("using")]
            return JsonResponse(data={"data": device_list, "current_page": current_page, "total": total,
                                      "page_sizes": page_size}, code="999999", msg="查询device列表成功")
        elif d_type == 'api':
            for device in serialize.data:
                info = dict(desc=device['desc'], port=device['port'], bootstrapPort=device['bootstrapPort'],
                            desired_caps=dict(platformName=device['platformName'], deviceName=device['deviceName'],
                                              noReset=device['noReset'],
                                              platformVersion=device['platformVersion'],
                                              automationName=device['automationName'],
                                              newCommandTimeout=device['newCommandTimeout']))
                device_list.append(info)
            return JsonResponse(data=device_list, code="999999", msg="查询device列表成功")


class EditDeviceView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        验证参数
        """
        # 必传参数
        for param in ["id", "desc", "port", "platformName", "deviceName", "noReset", "platformVersion",
                      "automationName",  "newCommandTimeout", "bootstrapPort"]:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空！")
        obj = Devices.objects.filter(id=data.get('id'))
        if not obj:
            return JsonResponse(code="999995", msg=f"记录{data.get('id')}不存在,请修改参数id!")
        if Devices.objects.filter(desc=data.get('desc')).exclude(id=data.get('id')):
            return JsonResponse(code="999995", msg=f"手机名称{data.get('desc')}已存在,请修改参数desc!")
        if Devices.objects.filter(deviceName=data.get('deviceName')).exclude(id=data.get('id')):
            return JsonResponse(code="999995", msg=f"设备名称{data.get('deviceName')}已存在,请修改参数deviceName!")
        if Devices.objects.filter(port=data.get('port')).exclude(id=data.get('id')):
            return JsonResponse(code="999995", msg=f"服务端口{data.get('port')}已存在,请修改参数port!")
        if Devices.objects.filter(bootstrapPort=data.get('bootstrapPort')).exclude(id=data.get('id')):
            return JsonResponse(code="999995", msg=f"交互端口{data.get('bootstrapPort')}已存在,请修改参数bootstrapPort!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=["desc", "port", "platformName", "deviceName", "noReset", "platformVersion",
                  "automationName", "newCommandTimeout", "bootstrapPort"],
        properties={
            'desc': openapi.Schema(type=openapi.TYPE_STRING, description="手机备注"),
            'port': openapi.Schema(type=openapi.TYPE_STRING, description="appium服务端口"),
            'platformName': openapi.Schema(type=openapi.TYPE_STRING, description="手机系统"),
            'deviceName': openapi.Schema(type=openapi.TYPE_STRING, description="设备名称"),
            'noReset': openapi.Schema(type=openapi.TYPE_STRING, description="是否充值"),
            'platformVersion': openapi.Schema(type=openapi.TYPE_STRING, description="系统版本"),
            'automationName': openapi.Schema(type=openapi.TYPE_STRING, description="自动化名称"),
            'newCommandTimeout': openapi.Schema(type=openapi.TYPE_STRING, description="超时时间"),
            'bootstrapPort': openapi.Schema(type=openapi.TYPE_STRING, description="设备与appium交互端口"),
        },
    ))
    @catch_exception
    def post(self, request):
        """
        手机device信息修改
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        obj = Devices.objects.get(id=data["id"])
        data['lastUpdateTime'] = datetime.now()
        serializer = DeviceDeserializer(data=data)
        with transaction.atomic():
            if serializer.is_valid():
                serializer.update(instance=obj, validated_data=data)
                return JsonResponse(code="999999", msg="设备信息编辑成功")
            else:
                return JsonResponse(code="999998", msg="设备信息编辑失败", data=serializer.errors)


class GetDesiredCapsView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def get_account_info(account_list, device):

        for account in account_list:
            if account.get('device') == device:
                return account.get('username'), account.get('password')

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'app': openapi.Schema(type=openapi.TYPE_STRING, description="应用名称"),
            'suite': openapi.Schema(type=openapi.TYPE_STRING, description="用例集"),
            'account':  openapi.Schema(type=openapi.TYPE_ARRAY, description="执行参数",
                                          items=openapi.Schema(type=openapi.TYPE_OBJECT, properties={
                                              'username': openapi.Schema(type=openapi.TYPE_STRING, description="用户名"),
                                              'password': openapi.Schema(type=openapi.TYPE_STRING, description="用户密码"),
                                              'device': openapi.Schema(type=openapi.TYPE_STRING, description="设备ID")

                                          }),
                                          default="[{'username': '用户名', 'password': 用户密码, 'device': 设备ID ]"),
        },
    ))
    @catch_exception
    def post(self, request):
        """
        获取执行用例设备相关配置
        """
        data = JSONParser().parse(request)
        accounts = data.get('account')
        device_arr = [account.get('device') for account in accounts]
        # app, suite, username, password, device_name = data.get("app"), data.get("suite"), account.get("username"), account.get("password"), account.get("device")
        app, suite = data.get("app"), data.get("suite")
        obj = Devices.objects.filter(deviceName__in=device_arr)
        if not obj:
            return JsonResponse(code="999995", msg=f"获取设备配置失败,请检查设备ID:{','.join(device_arr)}信息是否存在!")
        devices = DeviceSerializer(obj, many=True).data
        app_obj = App.objects.filter(name=app).first()
        device_list = []
        for device in devices:
            username, password = self.get_account_info(accounts, device.get('deviceName'))
            info = dict(desc=device['desc'], port=device['port'], bootstrapPort=device['bootstrapPort'],
                        desired_caps=dict(platformName=device['platformName'], deviceName=device['deviceName'],
                                          noReset=device['noReset'],
                                          appPackage=app_obj.appPackage,
                                          appActivity=app_obj.appActivity,
                                          platformVersion=device['platformVersion'],
                                          automationName=device['automationName'],
                                          newCommandTimeout=device['newCommandTimeout']),
                        username=username, password=password)
            device_list.append(info)
        return JsonResponse(data=device_list, code="999999", msg="查询device列表成功")


class OpenStfLogin(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="用户名"),
            'email': openapi.Schema(type=openapi.TYPE_STRING, description="邮箱")
        },
    ))
    @catch_exception
    def post(self, request):
        """
        openStf模拟登录
        """
        data = JSONParser().parse(request)
        login = {"name": data.get("name", "admin"), "email": data.get("email", "admin@126.com")}
        url = "http://192.168.29.34:7100/auth/api/v1/mock"
        headers = {
                    'X-XSRF-TOKEN': 'vl1QPMKK-xrfIcuU-SoXD5JrwTJMSMGSUkMk',
                    'Cookie': 'ssid=eyJjc3JmU2VjcmV0IjoiNzRCUWNFNFlKak9sQjNUREk0WjdwMktGIn0=; ssid.sig=7L85TUlNjMCE4g36Q4P7Sy6eaEc; XSRF-TOKEN=vl1QPMKK-xrfIcuU-SoXD5JrwTJMSMGSUkMk' }
        result = requests.post(url=url, json=login, headers=headers)
        if result.json().get('success'):
            return JsonResponse(data=result.json(), code="999999", msg="获取openstf登录信息成功")
        else:
            return JsonResponse(data=result.json(), code="999995", msg="获取openstf登录信息失败")


class GetDeviceInfoView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def get_device_info():
        url = f'http://192.168.29.34:7100/api/v1/devices'
        token = '0edf9f380f7c41f2803066d16e4b8805fc9a318098554f82aaabc3f2f5aaac71'
        headers = {'Authorization': 'Bearer ' + token}
        requests.DEFAULT_RETRIES = 5
        s = requests.session()
        s.keep_alive = False
        response = requests.get(url, headers=headers)
        return response.json()

    @catch_exception
    def post(self, request):
        """
        根据stf接口获取手机device信息
        """
        response = self.get_device_info()
        device_list = list()
        if response.get('success'):
            for device in response.get('devices'):
                if '5555' not in device['serial']:
                    device_info = dict()
                    device_info['deviceName'] = device['serial']
                    device_info['model'] = device['model']
                    device_info['platformName'] = device['platform']
                    device_info['platformVersion'] = device['version']
                    device_info['desc'] = device['marketName']
                    device_info['screen'] = f"{device['display']['height']}*{device['display']['width']}"
                    device_info['image'] = 'https://inside.testin.cn/cfgfiles/images/model/1626252466908/3458a354d57e4b769f0769d76f885b7e.jpg'
                    device_list.append(device_info)
        return JsonResponse(data=device_list, code="999999", msg="查询device信息成功")
