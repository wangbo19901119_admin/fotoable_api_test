import logging
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from api_test.common.api_response import JsonResponse
from api_test.models import ApiVersionLevelFirst
from api_test.serializers import ApiVersionLevelFirstSerializer, ApiVersionLevelFirstDeserializer

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class Version(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        # openapi.Parameter(name="project_id", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="项目id"),
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="版本名称"),
    ])
    def get(self, request):
        """
        接口版本
        """
        # project_id = request.GET.get("project_id")
        try:
            page_size = int(request.GET.get("page_size", 1000))
            current_page = int(request.GET.get("page", 1))
        except (TypeError, ValueError):
            return JsonResponse(code="999985", msg="page and page_size must be integer!")
        name = request.GET.get("name")

        # 校验参数
        # if not project_id:
        #     return JsonResponse(code="999996", msg="参数有误!")
        # if not project_id.isdecimal():
        #     return JsonResponse(code="999996", msg="参数有误!")
        # 验证项目是否存在
        # try:
        #     pro_data = Project.objects.get(id=project_id, status=True)
        # except ObjectDoesNotExist:
        #     return JsonResponse(code="999995", msg="项目不存在!")
        # # 序列化结果
        # pro_data = ProjectListSerializer(pro_data)
        # # 校验项目状态
        # if not pro_data.data["status"]:
        #     return JsonResponse(code="999985", msg="该项目已禁用")
        # 查找项目下所有接口信息，并按id排序，序列化结果
        obi = ApiVersionLevelFirst.objects.filter(status=True)
        if name:
            obi = obi.filter(name__icontains=name)
        obi = obi.order_by("id")
        paginator = Paginator(obi, page_size)  # paginator对象
        page_sizes = paginator.num_pages  # 总页数
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = ApiVersionLevelFirstSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": current_page,
                                  "total": total,
                                  "page_sizes": page_sizes,
                                  }, code="999999", msg="成功")


class AddVersion(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 必传参数 name
            if not data.get("name"):
                return JsonResponse(code="999996", msg="参数有误!")
            # 校验project_id类型为int
            # if not isinstance(data["project_id"], int):
            #     return JsonResponse(code="999996", msg="参数有误!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['name'],
        properties={
            # 'project_id': openapi.Schema(type=openapi.TYPE_INTEGER, description="项目id"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="接口版本名称"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="接口版本描述")
        },
    ))
    def post(self, request):
        """
        新增接口版本
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        # 校验项目状态
        # try:
        #     obj = Project.objects.get(id=data["project_id"])
        #     if not request.user.is_superuser and obj.user.is_superuser:
        #         return JsonResponse(code="999983", msg="无操作权限！")
        # except ObjectDoesNotExist:
        #     return JsonResponse(code="999995", msg="项目不存在!")
        # pro_data = ProjectListSerializer(obj)
        # if not pro_data.data["status"]:
        #     return JsonResponse(code="999985", msg="该项目已禁用")
        count = ApiVersionLevelFirst.objects.filter(name=data["name"]).count()
        if count > 0:
            return JsonResponse(code="999997", msg="存在相同名称！")
        else:
            # 反序列化
            serializer = ApiVersionLevelFirstDeserializer(data=data)
            # 校验反序列化正确，正确则保存，外键为project
            if serializer.is_valid():
                serializer.save()
            else:
                return JsonResponse(code="999998", msg="新增版本失败!")
            # 新增接口操作
            # record_dynamic(project=serializer.data.get("project_id"),
            #                _type="添加", operationObject="接口版本", user=request.user.pk,
            #                data="新增接口版本“%s”" % data["name"])
            return JsonResponse(data={
                "version_id": serializer.data.get("id")
            }, code="999999", msg="新增版本成功!")


class UpdateNameVersion(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 必传参数 name
            if not data.get("id") or not data["name"]:
                return JsonResponse(code="999996", msg="参数有误!")
            # 校验project_id, id类型为int
            if not isinstance(data["id"], int):
                return JsonResponse(code="999996", msg="参数有误!")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'name'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="接口版本id"),
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="接口版本名称"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="接口版本描述")
        },
    ))
    def post(self, request):
        """
        修改接口版本名称
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        # try:
        #     pro_data = Project.objects.get(id=data["project_id"])
        #     if not request.user.is_superuser and pro_data.user.is_superuser:
        #         return JsonResponse(code="999983", msg="无操作权限！")
        # except ObjectDoesNotExist:
        #     return JsonResponse(code="999995", msg="项目不存在!")
        # pro_data = ProjectListSerializer(pro_data)
        # if not pro_data.data["status"]:
        #     return JsonResponse(code="999985", msg="该项目已禁用")
        try:
            obj = ApiVersionLevelFirst.objects.get(id=data["id"])
        except ObjectDoesNotExist:
            return JsonResponse(code="999991", msg="版本不存在!")
        serializer = ApiVersionLevelFirstDeserializer(data=data)
        if serializer.is_valid():
            serializer.update(instance=obj, validated_data=data)
        else:
            return JsonResponse(code="999998", msg="失败!")
        # record_dynamic(project=data.get("project_id"),
        #                _type="修改", operationObject="接口版本", user=request.user.pk,
        #                data="修改接口版本“%s”" % data["name"])
        return JsonResponse(code="999999", msg="更新成功!")


class DelVersion(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 校验project_id, id类型为int
            if not isinstance(data["ids"], list):
                for i in data["ids"]:
                    if not isinstance(i, int) or not isinstance(int(i), int):
                        return JsonResponse(code="999995", msg="参数有误！")
                return JsonResponse(code="999995", msg="参数有误！")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['ids'],
        properties={
            'ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                  description="待删除待接口版本 id列表"),
        },
    ))
    def post(self, request):
        """
        删除(禁用)接口版本名称
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        # try:
        #     pro_data = Project.objects.get(id=data["project_id"])
        #     if not request.user.is_superuser and pro_data.user.is_superuser:
        #         return JsonResponse(code="999983", msg="无操作权限！")
        # except ObjectDoesNotExist:
        #     return JsonResponse(code="999995", msg="项目不存在!")
        # pro_data = ProjectListSerializer(pro_data)
        # if not pro_data.data["status"]:
        #     return JsonResponse(code="999985", msg="该项目已禁用")
        # 根据项目id和模块 id查找，若存在则删除
        try:
            for j in data["ids"]:
                obj = ApiVersionLevelFirst.objects.get(id=j)
                if obj:
                    # obj.delete()  # 删除
                    obj.status = False
                    obj.save()
                    # record_dynamic(project=data["project_id"],
                    #     _type="禁用", operationObject="接口版本", user=request.user.pk, data="禁用接口版本“%s”" % obj.name)
            return JsonResponse(code="999999", msg="删除成功！")
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg="版本不存在！")
