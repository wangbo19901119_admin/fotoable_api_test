import logging
import os
from datetime import datetime, date
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from django.db.models import Q
from django.http import FileResponse
from django_bulk_update.helper import bulk_update
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from api_test.common.api_response import JsonResponse
from api_test.models import Demand, DemandReport
from api_test.serializers import DemandDeserializer, DemandSerializer, DemandReportDeSerializer, DemandReportSerializer
from dofun_test_center_backend import settings

logger = logging.getLogger(__name__)  # 这里使用 __name__ 动态搜索定义的 logger 配置，这里有一个层次关系的知识点。


class DemandListView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="page", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="当前页数"),
        openapi.Parameter(name="page_size", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                          description="每页最多显示的项目数"),
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="项目名称"),
        openapi.Parameter(name="manager", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="负责人"),
        openapi.Parameter(name="status", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="项目状态"),
        openapi.Parameter(name="startTime", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="开始时间"),
        openapi.Parameter(name="endTime", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="结束时间"),
        openapi.Parameter(name="releaseStartTime", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="查询上线开始时间"),
        openapi.Parameter(name="releaseEndTime", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="查询上线结束时间"),

    ])
    def get(self, request):
        """
        获取项目需求排期列表
        """
        page_size = int(request.GET.get("page_size", 10))
        current_page = int(request.GET.get("page", 1))
        name = request.GET.get("name")
        manager = request.GET.get("manager")
        status = request.GET.get("status")
        start = request.GET.get('startTime')
        end = request.GET.get('endTime')
        level = request.GET.get('level')
        release_start = request.GET.get('releaseStartTime')
        release_end = request.GET.get('releaseEndTime')
        # 获取当前用户有查看项目权限的项目列表
        obi = Demand.objects.filter(state=True)
        if name:  # 查询name
            obi = obi.filter(name__icontains=name)
        if manager:  # 查询负责人
            obi = obi.filter(manager__contains=manager)
        if status:  # 查询状态
            obi = obi.filter(status=status)
        if start and end:
            obi = obi.filter(Q(startTime__range=(start, end)) | Q(endTime__range=(start, end)) | Q(startTime__lt=start, endTime__gt=end) | Q(status='未开始') | Q(releaseTime__range=(start, end)))
        if release_start and release_end:
            obi = obi.filter(Q(releaseTime__range=(release_start, release_end)))
        if level:
            obi = obi.filter(level=level)
        obi = obi.order_by("-status", "-id")  # 仅展示status=1的项目
        if not obi:
            return JsonResponse(data={"data": [],
                                      "current_page": 1,
                                      "total": 0,
                                      "page_sizes": page_size,
                                      }, code="999999", msg="暂无项目排期！")

        paginator = Paginator(obi, page_size)  # paginator对象
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = DemandSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": current_page,
                                  "total": total,
                                  "page_sizes": page_size,
                                  }, code="999999", msg="查询项目排期成功")


class AddDemandView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)  #

    @staticmethod
    def parameter_check(data):
        """
        验证参数
        """
        # 必传参数 name, protocol, structure
        for param in ['name', 'manager', 'status', 'level']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空！")
        for param in ['releaseTime', 'startTime', 'endTime']:
            try:
                if data.get(param):
                    time = datetime.strptime(data.get(param), "%Y-%m-%d")
                    data[param] = date(year=time.year, month=time.month, day=time.day)
            except ValueError:
                return JsonResponse(code="999996", msg=f"参数{param}格式错误！")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['name', 'manager', 'status', 'estimateTime', 'realTime', 'level'],
        properties={
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="项目名称"),
            'manager': openapi.Schema(type=openapi.TYPE_STRING, description="项目负责人"),
            'status': openapi.Schema(type=openapi.TYPE_STRING, description="项目状态"),
            'level': openapi.Schema(type=openapi.TYPE_STRING, description="项目优先级"),
            'estimateTime': openapi.Schema(type=openapi.TYPE_INTEGER, description="预计天数"),
            'realTime': openapi.Schema(type=openapi.TYPE_INTEGER, description="实际天数"),
            'releaseTime': openapi.Schema(type=openapi.TYPE_INTEGER, description="上线日期"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="项目备注"),
        },
    ))
    def post(self, request):
        """
        排期项目新增
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        data['manager'] = str(data['manager'])
        project_serializer = DemandDeserializer(data=data)
        try:
            Demand.objects.get(name=data["name"], state=True)
            return JsonResponse(code="999997", msg="存在相同名称")
        except ObjectDoesNotExist:
            with transaction.atomic():
                if project_serializer.is_valid():
                    project_serializer.save()
                    pid = project_serializer.data.get("id")
                    return JsonResponse(data={
                        "id": pid
                    }, code="999999", msg="排期项目新成功")
                else:
                    return JsonResponse(code="999998", msg="排期项目新增失败", data={"errmsg": project_serializer.errors})


class EditDemandView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['id', 'name', 'manager', 'status', 'level']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空！")
        for param in ['releaseTime', 'startTime', 'endTime']:
            try:
                if data.get(param):
                    datetime.strptime(data.get(param), "%Y-%m-%d")
            except ValueError:
                return JsonResponse(code="999996", msg=f"参数{param}格式错误！")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['name', 'manager', 'status', 'estimateTime', 'realTime', 'level'],
        properties={
            'name': openapi.Schema(type=openapi.TYPE_STRING, description="项目名称"),
            'manager': openapi.Schema(type=openapi.TYPE_STRING, description="项目负责人"),
            'status': openapi.Schema(type=openapi.TYPE_STRING, description="项目状态"),
            'level': openapi.Schema(type=openapi.TYPE_STRING, description="项目优先级"),
            'estimateTime': openapi.Schema(type=openapi.TYPE_INTEGER, description="预计天数"),
            'realTime': openapi.Schema(type=openapi.TYPE_INTEGER, description="实际天数"),
            'releaseTime': openapi.Schema(type=openapi.TYPE_STRING, description="上线日期"),
            'startTime': openapi.Schema(type=openapi.TYPE_STRING, description="开始日期"),
            'endTime': openapi.Schema(type=openapi.TYPE_STRING, description="结束日期"),
            'description': openapi.Schema(type=openapi.TYPE_STRING, description="项目备注"),
        },
    ))
    def post(self, request):
        """
        修改排期需求
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        data['manager'] = str(data['manager'])
        if result:
            return result
        try:
            obj = Demand.objects.get(id=data["id"])
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg=f"需求{id}不存在！")
        # 查找是否相同名称的项目
        count = Demand.objects.filter(name=data["name"], state=True).exclude(id=data["id"]).count()
        if count > 0:
            return JsonResponse(code="999997", msg="存在相同名称")
        else:
            serializer = DemandDeserializer(data=data)
            with transaction.atomic():
                if serializer.is_valid():
                    # 修改项目
                    serializer.update(instance=obj, validated_data=data)
                    # 记录动态
                    return JsonResponse(code="999999", msg="排期需求编辑成功")
                else:
                    return JsonResponse(code="999998", msg="排期需求编辑失败", data=serializer.errors)


class DelDemandView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        try:
            # 校验demand_id类型为int
            if not data.get("idList") or not isinstance(data["idList"], list):
                return JsonResponse(code="999996", msg="参数有误！")
            for i in data["idList"]:
                if not isinstance(i, int) and not isinstance(int(i), int):  # 支持字符串格式的整数
                    return JsonResponse(code="999996", msg="参数有误！")
        except KeyError:
            return JsonResponse(code="999996", msg="参数有误！")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['idList'],
        properties={
            'idList': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(type=openapi.TYPE_INTEGER),
                                     description="待删除待需求id列表"),
        },
    ))
    def post(self, request):
        """
        删除需求
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        try:
            for i in data["idList"]:
                try:
                    Demand.objects.get(id=i)
                except ObjectDoesNotExist:
                    return JsonResponse(code="999995", msg=f"需求不存在,删除成功！")
            obj_list = Demand.objects.filter(id__in=data.get('idList'))
            for obj in obj_list:
                obj.state = False
            bulk_update(obj_list, update_fields=['state'])
            return JsonResponse(code="999999", msg="需求删除成功")
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg="需求不存在,删除成功！")


class UploadDemandReport(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data, demand_id):
        """
        校验参数
        """
        # 校验data
        if not data or not demand_id:
            return JsonResponse(code="999996", msg="参数有误!")

    @staticmethod
    def update_report_id(demand_id, report_id):
        Demand.objects.filter(id=demand_id).update(report_id=report_id)

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'file'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="排期需求id"),
            'file': openapi.Schema(type=openapi.TYPE_FILE, description="测试报告"),
            'type': openapi.Schema(type=openapi.TYPE_STRING, description="测试类型")

        },
    ))
    def post(self, request):
        """
        上传测试报告
        """
        data = request.FILES.get('file')
        desc = request.data.get('desc', None)
        dtype = request.data.get('type', 'APP')
        demand_id = request.data.get('id')
        file_name = data.name
        result = self.parameter_check(data, demand_id)
        if result:
            return result
        try:
            demand = Demand.objects.get(id=demand_id)
        except ObjectDoesNotExist:
            return JsonResponse(code="999995", msg=f"需求不存在,删除成功！")
        if demand.report_id:
            report = DemandReport.objects.filter(id=demand.report_id)
            file_full_name = os.path.join(settings.MEDIA_ROOT, report[0].file.__str__())
            params = {'name': file_name, 'file': data, 'desc': desc, 'type': dtype} if report[0].status else {'name': file_name, 'file': data, 'desc': desc, 'status': True, 'type': dtype}
            param_serialize = DemandReportDeSerializer(data=params)
            if param_serialize.is_valid():
                if os.path.isfile(file_full_name):
                    os.remove(file_full_name)
                param_serialize.update(instance=report[0], validated_data=params)
                self.update_report_id(demand_id, report[0].id)
                return JsonResponse(code="999999", msg="报告上传成功")
            else:
                return JsonResponse(code="999995", msg="报告上传失败", data=param_serialize.errors)
        else:
            params = {'name': file_name, 'file': data, 'desc': desc, 'type': dtype}
            file_serializer = DemandReportDeSerializer(data=params)
            if file_serializer.is_valid():
                file_serializer.save()
                self.update_report_id(demand_id, file_serializer.data.get('id'))
                return JsonResponse(code="999999", msg="报告上传成功")
            else:
                return JsonResponse(code="999999", msg="报告上传失败", data=file_serializer.errors)


class DemandReportList(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="page", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="当前页数"),
        openapi.Parameter(name="page_size", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER,
                          description="每页最多显示的项目数"),
        openapi.Parameter(name="name", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="报告名称"),
        openapi.Parameter(name="type", in_=openapi.IN_QUERY, type=openapi.TYPE_STRING, description="报告类型"),

    ])
    def get(self, request):
        """
        获取排期需求报告列表
        """
        try:
            page_size = int(request.GET.get("page_size", 10))
            current_page = int(request.GET.get("page", 1))
        except (TypeError, ValueError):
            return JsonResponse(code="999985", msg="page and page_size must be integer!")
        name = request.GET.get("name")
        rtype = request.GET.get("type")
        # 获取当前用户有查看项目权限的项目列表
        obi = DemandReport.objects.filter(status=True)
        if name:  # 查询的name
            obi = obi.filter(name__icontains=name)
        if rtype:
            obi = obi.filter(type=rtype)
        obi = obi.order_by("-createTime")  # 仅展示status=1的项目
        paginator = Paginator(obi, page_size)  # paginator对象
        page_sizes = paginator.num_pages  # 总页数
        total = paginator.count  # 总数
        try:
            obm = paginator.page(current_page)
        except PageNotAnInteger:
            obm = paginator.page(1)
        except EmptyPage:
            obm = paginator.page(paginator.num_pages)
        serialize = DemandReportSerializer(obm, many=True)
        return JsonResponse(data={"data": serialize.data,
                                  "current_page": current_page,
                                  "total": total,
                                  "page_sizes": page_sizes,
                                  }, code="999999", msg="成功")


class DownloadReport(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @swagger_auto_schema(manual_parameters=[
        openapi.Parameter(name="id", in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER, description="文件id"),
    ])
    def get(self, request):
        """
        下载单个文件
        """
        fid = request.GET.get("id")

        file_load = DemandReport.objects.get(id=fid, status=True)
        file = open(file_load.file.path, 'rb')
        file_name = file_load.file.name.split("/")[1]
        response = FileResponse(file)
        response['Content-Type'] = 'application/octet-stream'
        response['Content-Disposition'] = 'attachment;filename="%s"' % file_name
        return response


class UpdateReportView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        # 校验data
        for i in ['id', 'type']:
            if not data.get(i):
                return JsonResponse(code="999996", msg=f"参数{i}缺失!")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['id', 'type'],
        properties={
            'id': openapi.Schema(type=openapi.TYPE_INTEGER, description="报告id"),
            'type': openapi.Schema(type=openapi.TYPE_STRING, description="所属类型")
        },
    ))
    def post(self, request):
        """
        更新报告所属类型
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        rid = data.get('id')
        rtype = data.get('type')
        desc = data.get('desc')
        obi = DemandReport.objects.filter(id=rid, status=True)
        if not obi:
            return JsonResponse(code="999995", msg=f"报告{rid}不存在或已删除!")
        with transaction.atomic():
            obi.update(type=rtype, desc=desc)
            return JsonResponse(code="999999", msg="报告类型更新成功")


class EditDemandStartDateView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = ()

    @staticmethod
    def parameter_check(data):
        """
        校验参数
        """
        for param in ['start', 'id']:
            if not data.get(param):
                return JsonResponse(code="999996", msg=f"参数{param}不能为空！")

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=['start', 'id'],
        properties={
            'id':  openapi.Schema(type=openapi.TYPE_INTEGER, description="需求id"),
            'start': openapi.Schema(type=openapi.TYPE_STRING, description="开始日期"),
        },
    ))
    def post(self, request):
        """
        修改排期需求
        """
        data = JSONParser().parse(request)
        result = self.parameter_check(data)
        if result:
            return result
        obj = Demand.objects.filter(id=data["id"])
        if not obj:
            return JsonResponse(code="999995", msg=f"需求{id}不存在！")
        # 查找是否相同名称的项目
        with transaction.atomic():
            # 修改项目
            obj.update(startTime=data['start'])
            # 记录动态
            return JsonResponse(code="999999", msg="排期需求编辑成功")
