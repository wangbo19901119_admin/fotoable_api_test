*** Settings ***
Documentation    Suite description
Library          api_test.common.test_plan_case_processor.TestPlanCaseProcessor


*** Keywords ***
Log Response Info
    ${status_code} =  get status code
    ${req_url} =  get req url
    ${req_body} =  get req body
    ${resp_json} =  get resp json
    ${resp_headers} =  get resp headers
