import json
from api_test.testdb.test_model import TestModel
from api_test.common.decorator import close_db_connection, update_db_operation


class TestPlanCase(TestModel):
    def __init__(self):
        self.session = self.session()

    @close_db_connection
    def select_all_data(self):
        query = self.session.query(self.TestPlanCase).all()
        return query

    @update_db_operation
    def add_all(self, obj_list):
        data = [self.TestPlanCase(service=obj['service'], api_name=obj['api_name'], req_json=obj.get('req_json', {}),
                type=obj['http_type'], protocol=obj['protocol'], params=obj.get('params'), file=obj.get('file'),
                is_success=False) for obj in obj_list]
        self.session.add_all(data)

    @update_db_operation
    def add(self, service, api_name, http_type, protocol, req_json=json.dumps({}), params=None, file=None):
        data = self.TestPlanCase(service=service, api_name=api_name, req_json=req_json,
                                 type=http_type, protocol=protocol, params=params, file=file,
                                 is_success=False)
        self.session.add(data)

    @update_db_operation
    def update_test_data_by_name(self, api_name, **kwargs):
        self.session.query(self.TestPlanCase).filter_by(api_name=api_name).update(kwargs)

    @close_db_connection
    def select_data_by_name(self, name):
        query = self.session.query(self.TestPlanCase).filter_by(api_name=name).first()
        return query

    @update_db_operation
    def delete_all_data(self):
        self.session.execute('truncate api_test_testplanexecutecaselist')
