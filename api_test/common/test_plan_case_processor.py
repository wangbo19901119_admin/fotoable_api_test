import json
from json import JSONDecodeError
import requests
from jsonpath_rw import parser
from api_test import testdb
from api_test.common import const
from api_test.common.util import Util


class TestPlanCaseProcessor(object):

    def __init__(self) -> None:
        self.resp = None
        self.req_body = None
        self.api_name = None
        self.http_type = None
        self.status_code = None
        self.headers = None
        self.params_path = []

        # 内部函数用于生成post
        def add_post_method(cls, m_name, url) -> None:
            def inner(self, *args, **kwargs):
                inner.__name__ = m_name
                test_data = testdb.TestPlanCase().select_data_by_name(m_name)
                raw_data = json.loads(test_data.req_json)
                params = testdb.TestPlanCase().select_data_by_name(m_name).params
                files_data = testdb.TestPlanCase().select_data_by_name(m_name).file
                if not files_data:
                    try:
                        if isinstance(kwargs['headers'], str):
                            kwargs['headers'] = eval(kwargs['headers'])
                            if not isinstance(kwargs['headers'], dict):
                                raise TypeError(
                                    'Given string is not dict-like')

                            result = requests.post(url.format(*args),
                                                   data=json.dumps(raw_data),
                                                   headers=kwargs['headers'],
                                                   params=params)
                    except KeyError:
                        result = requests.post(url.format(
                            *args), data=json.dumps(raw_data), params=params)
                else:
                    try:
                        if isinstance(kwargs['headers'], str):
                            kwargs['headers'] = eval(kwargs['headers'])
                            if not isinstance(kwargs['headers'], dict):
                                raise TypeError(
                                    'Given string is not dict-like')
                            files_dict = json.loads(files_data)
                            files_data = {}
                            for key, value in files_dict.items():
                                if isinstance(value, list):
                                    try:
                                        files_data[key] = (value[0], open(value[1], 'rb'), value[2])
                                    except IndexError:
                                        raise IndexError(
                                            "file tuple must contains 3 item, filename, filepath and content-type")
                                else:
                                    files_data[key] = open(value, 'rb')
                            result = requests.post(url.format(*args),
                                                   files=files_data,
                                                   headers=kwargs['headers'],
                                                   params=params)
                    except KeyError:
                        files_dict = json.loads(files_data)
                        files_data = {key: open(value, 'rb') for key, value in files_dict.items()}
                        result = requests.post(url.format(
                            *args), files=files_data, params=params)
                self.resp = result
                response = self.get_resp_json()
                if self.resp.status_code != 200:
                    testdb.TestPlanCase().update_test_data_by_name(m_name, resp_json=result.text, is_success=False)
                else:
                    if isinstance(response, dict):
                        if response.get('code') != '999999':
                            resp = json.dumps(response)
                            is_success = False
                        else:
                            resp = json.dumps(response.get('data').get('data'))
                            code = response.get('data').get('status')
                            is_success = True if code in ['200 OK', '101 OK'] else False

                    testdb.TestPlanCase().update_test_data_by_name(m_name, resp_json=resp, is_success=is_success)
                self.req_body = json.loads(test_data.req_json)
                self.api_name = inner.__name__
            setattr(cls, m_name, inner)

        # 批量生成请求的方法
        reqs = testdb.TestPlanCase().select_all_data()
        for req in reqs:
            service_name = req.service
            api_name = req.api_name
            protocol = 'http' if req.protocol.lower() in ['http', 'https'] else 'websocket'
            try:
                url = const.service[protocol][service_name]['url']
            except KeyError:
                raise KeyError("Please check service_conf file.")
            # 检查test_data表中type字段是否是post,如果是生成post方法并传入相应的请求数据
            if req.type.lower() == 'post':
                globals()[api_name] = api_name
                print("url:", url)
                add_post_method(self.__class__, api_name, url)

    def get_status_code(self):
        if self.resp:
            return self.resp.status_code

    def get_resp_json(self):
        try:
            resp_json = json.loads(self.resp.content)
        except JSONDecodeError:
            return self.resp.content
        return resp_json

    def get_req_url(self):
        if self.resp is not None:
            return self.resp.url

    def get_resp_headers(self):
        return self.resp.headers

    def get_req_body(self):
        if self.req_body:
            try:
                return json.loads(self.req_body)
            except TypeError:
                return self.req_body

    def parse_resp_json(self, json_path, index=0):
        json_path_expr = parser.parse(json_path)
        if isinstance(self.get_resp_json(), list):
            results = [match.value for match in json_path_expr.find(
                self.get_resp_json()[index])]
        else:
            results = [match.value for match in json_path_expr.find(
                self.get_resp_json())]
        if not results:
            raise KeyError('Key not found!')
        if len(results) == 1:
            return results[0]
        else:
            return results

    @staticmethod
    def update_req_json(api_name, **kwargs):
        request = json.loads(testdb.TestPlanCase().select_data_by_name(api_name).req_json)
        params = Util().get_request_params_path(request)
        for key, value in kwargs.items():
            for param in params:
                if key == param.get('key'):
                    try:
                        eval(param.get('path'))[key] = value
                        testdb.TestPlanCase().update_test_data_by_name(api_name, req_json=json.dumps(request))
                    except IndexError or KeyError as e:
                        print(e)
