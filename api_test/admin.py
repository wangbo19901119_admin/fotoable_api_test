# Register your models here.
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from guardian.admin import GuardedModelAdmin

from api_test.models import Project, GlobalHost, ApiGroupLevelFirst, ApiVersionLevelFirst, ApiInfo, \
    APIRequestHistory, ApiOperationHistory, ProjectDynamic, ProjectMember, UserProfile, ApiParameter, \
    ApiResponse, \
    VisitorsRecord, ApiStatusCode, ApiDataType, ApiDataStructure, ApiCaseInfo, HelpDocumentInfo

from django.contrib import admin
from django.utils.text import capfirst
from collections import OrderedDict as SortedDict


def find_model_index(name):
    count = 0
    for model, model_admin in admin.site._registry.items():
        if capfirst(model._meta.verbose_name_plural) == name:
            return count
        else:
            count += 1
    return count


def index_decorator(func):
    def inner(*args, **kwargs):
        template_response = func(*args, **kwargs)
        for app in template_response.context_data['app_list']:
            app['models'].sort(key=lambda x: find_model_index(x['name']))
        return template_response

    return inner


registry = SortedDict()
registry.update(admin.site._registry)
admin.site._registry = registry
admin.site.index = index_decorator(admin.site.index)
admin.site.app_index = index_decorator(admin.site.app_index)
admin.site.site_header = '测试平台后台管理'
admin.site.siteTitle = '后台管理'

display = ()


class ProfileInline(admin.TabularInline):
    """
    用户模块扩展
    """
    model = UserProfile


class PositionForm(GuardedModelAdmin):
    fieldsets = ([
                     '职位', {
            'fields': ('position',)
        }],)


class CustomUserAdmin(UserAdmin):
    inlines = [ProfileInline, ]


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)


class ReadOnlyModelAdmin(GuardedModelAdmin):
    """ModelAdmin class that prevents modifications through the admin.

    The changelist and the detail view work, but a 403 is returned
    if one actually tries to edit an object.
    """

    actions = None

    def get_readonly_fields(self, request, obj=None):
        return self.fields or [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False

    # Allow viewing objects but not actually changing them
    def has_change_permission(self, request, obj=None):
        if request.method not in ('GET', 'HEAD'):
            return True
        return super(ReadOnlyModelAdmin, self).has_change_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        return False


class ReadAndDeleteModelAdmin(GuardedModelAdmin):
    """ModelAdmin class that prevents modifications through the admin.

    The changelist and the detail view work, but a 403 is returned
    if one actually tries to edit an object.
    """

    actions = None

    def get_readonly_fields(self, request, obj=None):
        return self.fields or [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False

    # Allow viewing objects but not actually changing them
    def has_change_permission(self, request, obj=None):
        if request.method not in ('GET', 'HEAD'):
            return True
        return super(ReadAndDeleteModelAdmin, self).has_change_permission(request, obj)


class MemberInProject(admin.TabularInline):
    model = ProjectMember

#
# class HostInProject(admin.TabularInline):
#     model = GlobalHost


class ProjectForm(GuardedModelAdmin):
    # inlines = [MemberInProject, HostInProject]
    inlines = [MemberInProject]
    search_fields = ('name', 'protocol')
    list_display = ('id', 'name', 'protocol', 'structure', 'status', 'LastUpdateTime', 'createTime', 'user')
    list_display_links = ('id', 'name',)
    list_filter = ('status', 'protocol')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '项目', {
            'fields': ('name', 'protocol', 'structure', 'description', 'status', 'user')
        }],
    )


admin.site.register(Project, ProjectForm)


class GlobalHostForm(GuardedModelAdmin):
    search_fields = ('name',)
    list_display = ('id', 'name', 'host', 'port', 'status')
    list_display_links = ('id', 'name', 'host', 'port')
    list_filter = ('status',)
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     'Host配置', {
            'fields': ('name', 'host', 'port', 'description', 'status')
        }],)


admin.site.register(GlobalHost, GlobalHostForm)


class CustomMethodForm(GuardedModelAdmin):
    search_fields = ('name',)
    list_display = ('id', 'project', 'name', 'description', 'type', 'status', 'dataCode')
    list_display_links = ('id', 'project', 'name')
    list_filter = ('project', 'type', 'status')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '自定义方法', {
            'fields': ('project', 'name', 'description', 'type', 'status', 'dataCode')
        }],)


class ApiGroupLevelFirstForm(GuardedModelAdmin):
    search_fields = ('name', 'status')
    list_display = ('id', 'name', 'status', 'sequence')
    list_display_links = ('id', 'name', 'status', 'sequence')
    list_per_page = 20
    ordering = ('sequence', 'id')
    fieldsets = ([
                     '接口模块', {
            'fields': ( 'name', 'status', 'sequence')
        }],)


admin.site.register(ApiGroupLevelFirst, ApiGroupLevelFirstForm)


class ApiVersionLevelFirstForm(GuardedModelAdmin):
    search_fields = ('name', 'status')
    list_display = ('id', 'name', 'status')
    list_display_links = ('id', 'name', 'status')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '接口版本', {
            'fields': ('name', 'status')
        }],)


admin.site.register(ApiVersionLevelFirst, ApiVersionLevelFirstForm)


class ApiStatusCodeForm(GuardedModelAdmin):
    search_fields = ('code', 'project', 'description')
    list_display = ('id', 'project', 'code', 'description', 'status')
    list_display_links = ('id', 'project', 'code', 'description', 'status')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '接口状态码', {
            'fields': ('project', 'code', 'description', 'status')
        }],)


admin.site.register(ApiStatusCode, ApiStatusCodeForm)


class DataTypeForm(GuardedModelAdmin):
    search_fields = ('type',)
    list_display = ('id', 'type', 'complex', 'description', 'status')
    list_display_links = ('id', 'type', 'description')
    list_filter = ('status', )
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '数据类型配置', {
            'fields': ('type', 'complex', 'description', 'status')
        }],)


admin.site.register(ApiDataType, DataTypeForm)


class DataStructureForm(GuardedModelAdmin):
    search_fields = ('name', 'type')
    list_display = ('id', 'type', 'name', 'type_sub', 'repeated', 'description',)
    list_display_links = ('id', 'type', 'name', 'type_sub', 'description')
    list_filter = ('name', 'type')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '复杂数据类型结构配置', {
            'fields': ('type', 'name', 'type_sub', 'repeated', 'description')
        }],)


admin.site.register(ApiDataStructure, DataStructureForm)


# class ApiHeadInline(admin.TabularInline):
#     model = ApiHead


class ApiParameterInline(admin.TabularInline):
    model = ApiParameter


class ApiResponseInline(admin.TabularInline):
    model = ApiResponse


class ApiInfoForm(GuardedModelAdmin):
    inlines = [ApiParameterInline, ApiResponseInline]
    search_fields = ('name', 'project', 'protocol', 'structure', 'requestType')
    list_display = ('id', 'project', 'name', 'protocol', 'structure', 'requestType',
                    'apiAddress', 'status', 'lastUpdateTime', 'userUpdate')
    list_display_links = ('id', 'name', 'project')
    list_filter = ('project', 'protocol', 'structure', 'requestType', 'status')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '接口信息', {
            'fields': ('project', 'apiModule', 'apiVersion', 'name', 'description', 'protocol',
                       'structure', 'requestType', 'apiAddress', 'status', 'userUpdate')
        }],)


admin.site.register(ApiInfo, ApiInfoForm)


class ApiCaseInfoForm(GuardedModelAdmin):
    search_fields = ('name', 'api')
    list_display = ('id', 'api', 'name', 'data', 'status', 'lastUpdateTime', 'userUpdate')
    list_display_links = ('id', 'name', 'api')
    list_filter = ('name', 'api', 'status')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '接口用例信息', {
            'fields': ('api', 'name', 'data', 'description', 'status', 'userUpdate')
        }],)


admin.site.register(ApiCaseInfo, ApiCaseInfoForm)


class APIRequestHistoryForm(ReadOnlyModelAdmin):
    search_fields = ('api', 'requestType', 'httpCode')
    list_display = ('id', 'api', 'requestType', 'requestAddress', 'httpCode', 'requestTime')
    list_display_links = ('id', 'api', 'requestTime')
    list_filter = ('requestType', 'httpCode')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '接口请求历史', {
            'fields': ('api', 'requestType', 'requestAddress', 'httpCode')
        }],)


admin.site.register(APIRequestHistory, APIRequestHistoryForm)


class ApiOperationHistoryForm(ReadOnlyModelAdmin):
    search_fields = ('api', 'user')
    list_display = ('id', 'api', 'user', 'description', 'time')
    list_display_links = ('id', 'api', 'user')
    list_filter = ('user',)
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '接口操作记录', {
            'fields': ('api', 'user', 'description')
        }],)


admin.site.register(ApiOperationHistory, ApiOperationHistoryForm)


class ProjectMemberForm(GuardedModelAdmin):
    search_fields = ('user', 'project')
    list_display = ('id', 'permissionType', 'project', 'user')
    list_display_links = ('permissionType', 'project')
    list_filter = ('permissionType', 'project', 'user')
    list_per_page = 20
    ordering = ('id',)
    fieldsets = ([
                     '项目成员', {
            'fields': ('permissionType', 'project', 'user')
        }],
    )


admin.site.register(ProjectMember, ProjectMemberForm)


class ProjectDynamicForm(ReadOnlyModelAdmin):
    search_fields = ('operationObject', 'user')
    list_display = ('id', 'project', 'time', 'type', 'operationObject', 'description', 'user', 'api')
    list_display_links = ('id', 'project', 'time')
    list_filter = ('project', 'type', 'api')
    list_per_page = 20
    ordering = ('-id',)


admin.site.register(ProjectDynamic, ProjectDynamicForm)


class VisitorsRecordForm(ReadAndDeleteModelAdmin):
    search_fields = ('province', 'city', 'district')
    list_display = ('id', 'formattedAddress', "country", "province", "city", "district", 'callTime')
    list_display_links = ('id', 'formattedAddress', "country", "province", "city", "district", 'callTime')
    list_per_page = 20
    ordering = ('-id',)


admin.site.register(VisitorsRecord, VisitorsRecordForm)


class HelpDocumentInfoForm(GuardedModelAdmin):
    # search_fields = ('name', 'project')
    list_display = ('id', 'help_document', 'status')
    list_display_links = ('id', 'help_document', 'status')
    list_filter = ('status',)
    list_per_page = 20
    # ordering = ('id',)
    fieldsets = ([
                     '帮助文档设置', {
            'fields': ('help_document', 'status')
        }],)


admin.site.register(HelpDocumentInfo, HelpDocumentInfoForm)
