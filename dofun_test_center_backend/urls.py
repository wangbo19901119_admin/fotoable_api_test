"""dofun_test_center_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from api_test import urls
from api_server import server_urls

# schema_view = get_schema_view(title='测试平台 API', renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer], permission_classes=())
from dofun_test_center_backend import settings

schema_view = get_schema_view(
   openapi.Info(
      title="测试平台接口API",
      default_version='v1',
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # url(r'^docs/', schema_view, name="docs"),
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    url(r'^api/', include(urls)),
    url(r'^server/', include(server_urls)),

    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    # path('mock/<path:apiAdr>', MockRequest.as_view()),
    url(r'^tinymce/', include('tinymce.urls')),
]

if settings.DEBUG:
    import os
    from django.conf.urls.static import static
    from django.conf import settings

    static_root = os.path.join(settings.BASE_DIR, 'collect_static')
    urlpatterns += static('/static', document_root=static_root)



# if settings.DEBUG:
#     import debug_toolbar
#     urlpatterns = [
#         path('__debug__/', include(debug_toolbar.urls)),
#     ] + urlpatterns
