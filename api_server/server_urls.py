from django.conf.urls import url

from api_server.dragonmaze.account import *
from api_server.dragonmaze.game import *
from api_server.mergical.game import *
from api_server.common.protocol import *
from api_server.tankhero.game import TankHeroProtocol
from api_server.townest.game import *

urlpatterns = [
    url(r'http/dragonmaze', DragonmazeAccount.as_view()),
    url(r'http/townest', TownestGame.as_view()),
    url(r'websocket/dragonmaze', DragonmazeGame.as_view()),
    url(r'websocket/mergical', MergicalGame.as_view()),
    url(r'websocket/json', WebsocketProtocol.as_view()),
    url(r'http/tankhero', TankHeroProtocol.as_view()),
    url(r'http', HttpProtocol.as_view()),

]

