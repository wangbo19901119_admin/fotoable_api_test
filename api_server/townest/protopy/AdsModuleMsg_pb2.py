# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: AdsModuleMsg.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='AdsModuleMsg.proto',
  package='',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x12\x41\x64sModuleMsg.proto\"\x10\n\x0e\x41\x64sInfoRequest\"Q\n\x0f\x41\x64sInfoResponse\x12\x10\n\x08\x61\x64sGroup\x18\x01 \x02(\t\x12\x14\n\x0clastViewTime\x18\x02 \x02(\x03\x12\x16\n\x0etodayViewCount\x18\x03 \x02(\x05\"\x12\n\x10\x41\x64sRewardRequest\"\x13\n\x11\x41\x64sRewardResponse*R\n\x16\x41\x64sModuleMsgSubCommand\x12\x1a\n\x15\x41\x44SMODULE_SUB_ADSINFO\x10\xc9\x65\x12\x1c\n\x17\x41\x44SMODULE_SUB_ADSREWARD\x10\xca\x65')
)

_ADSMODULEMSGSUBCOMMAND = _descriptor.EnumDescriptor(
  name='AdsModuleMsgSubCommand',
  full_name='AdsModuleMsgSubCommand',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='ADSMODULE_SUB_ADSINFO', index=0, number=13001,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ADSMODULE_SUB_ADSREWARD', index=1, number=13002,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=164,
  serialized_end=246,
)
_sym_db.RegisterEnumDescriptor(_ADSMODULEMSGSUBCOMMAND)

AdsModuleMsgSubCommand = enum_type_wrapper.EnumTypeWrapper(_ADSMODULEMSGSUBCOMMAND)
ADSMODULE_SUB_ADSINFO = 13001
ADSMODULE_SUB_ADSREWARD = 13002



_ADSINFOREQUEST = _descriptor.Descriptor(
  name='AdsInfoRequest',
  full_name='AdsInfoRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=22,
  serialized_end=38,
)


_ADSINFORESPONSE = _descriptor.Descriptor(
  name='AdsInfoResponse',
  full_name='AdsInfoResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='adsGroup', full_name='AdsInfoResponse.adsGroup', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='lastViewTime', full_name='AdsInfoResponse.lastViewTime', index=1,
      number=2, type=3, cpp_type=2, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='todayViewCount', full_name='AdsInfoResponse.todayViewCount', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=40,
  serialized_end=121,
)


_ADSREWARDREQUEST = _descriptor.Descriptor(
  name='AdsRewardRequest',
  full_name='AdsRewardRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=123,
  serialized_end=141,
)


_ADSREWARDRESPONSE = _descriptor.Descriptor(
  name='AdsRewardResponse',
  full_name='AdsRewardResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=143,
  serialized_end=162,
)

DESCRIPTOR.message_types_by_name['AdsInfoRequest'] = _ADSINFOREQUEST
DESCRIPTOR.message_types_by_name['AdsInfoResponse'] = _ADSINFORESPONSE
DESCRIPTOR.message_types_by_name['AdsRewardRequest'] = _ADSREWARDREQUEST
DESCRIPTOR.message_types_by_name['AdsRewardResponse'] = _ADSREWARDRESPONSE
DESCRIPTOR.enum_types_by_name['AdsModuleMsgSubCommand'] = _ADSMODULEMSGSUBCOMMAND
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

AdsInfoRequest = _reflection.GeneratedProtocolMessageType('AdsInfoRequest', (_message.Message,), dict(
  DESCRIPTOR = _ADSINFOREQUEST,
  __module__ = 'AdsModuleMsg_pb2'
  # @@protoc_insertion_point(class_scope:AdsInfoRequest)
  ))
_sym_db.RegisterMessage(AdsInfoRequest)

AdsInfoResponse = _reflection.GeneratedProtocolMessageType('AdsInfoResponse', (_message.Message,), dict(
  DESCRIPTOR = _ADSINFORESPONSE,
  __module__ = 'AdsModuleMsg_pb2'
  # @@protoc_insertion_point(class_scope:AdsInfoResponse)
  ))
_sym_db.RegisterMessage(AdsInfoResponse)

AdsRewardRequest = _reflection.GeneratedProtocolMessageType('AdsRewardRequest', (_message.Message,), dict(
  DESCRIPTOR = _ADSREWARDREQUEST,
  __module__ = 'AdsModuleMsg_pb2'
  # @@protoc_insertion_point(class_scope:AdsRewardRequest)
  ))
_sym_db.RegisterMessage(AdsRewardRequest)

AdsRewardResponse = _reflection.GeneratedProtocolMessageType('AdsRewardResponse', (_message.Message,), dict(
  DESCRIPTOR = _ADSREWARDRESPONSE,
  __module__ = 'AdsModuleMsg_pb2'
  # @@protoc_insertion_point(class_scope:AdsRewardResponse)
  ))
_sym_db.RegisterMessage(AdsRewardResponse)


# @@protoc_insertion_point(module_scope)
